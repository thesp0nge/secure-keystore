package it.codiceinsicuro.securekeystore;

import org.junit.Test;
import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class SecureKeystoreTest {
    @Test public void testCreateKeyStore() {
        SecureKeystore sk = new SecureKeystore("test.keystore", "test123");
        File f = new File("test.keystore");
        assertTrue(f.exists());

    }

		@Test public void testSetKey() {
        SecureKeystore sk = new SecureKeystore("test.keystore", "test123");
				boolean ret = sk.setKey("blog", "https://codiceinsicuro.it");
				assertTrue("setKey returns " + ret, ret);
				//assertTrue(sk.hasKey("blog"));
		}

		@Test public void testGetKey() {
        SecureKeystore sk = new SecureKeystore("test.keystore", "test123");
				assertTrue(sk.hasKey("blog"));
        String ret = sk.getKey("blog");
				assertEquals(ret, "https://codiceinsicuro.it");
		}
}
