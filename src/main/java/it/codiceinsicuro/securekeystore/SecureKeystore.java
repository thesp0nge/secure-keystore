package it.codiceinsicuro.securekeystore;

// Code used in https://codiceinsicuro.it/chicchi/keystore-non-solo-certificati/

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.UnrecoverableEntryException;

import java.io.File;
import java.nio.file.Files;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;

public class SecureKeystore {
	private KeyStore ks;
	private String 	 password;
	private String 	 filename;

	public SecureKeystore(String filename, String pwd) {

		try 
		{ 
			ks = createKeyStore(filename, pwd);
			createSaltFile(filename+".salt");
			this.password = pwd;
			this.filename = filename;
		}
			catch(NoSuchAlgorithmException | CertificateException | IOException | KeyStoreException e) {
			ks = null;
				// throwing a custom exception here
		}
	}
	
	private KeyStore createKeyStore(String fileName, String pw) throws NoSuchAlgorithmException, CertificateException, IOException, KeyStoreException {
    File file = new File(fileName);

    final KeyStore keyStore = KeyStore.getInstance("JCEKS");
    if (file.exists()) {
      keyStore.load(new FileInputStream(file), pw.toCharArray());
    } else {
      keyStore.load(null, null);
      keyStore.store(new FileOutputStream(fileName), pw.toCharArray());
    }

    return keyStore;
  }
	private void createSaltFile(String filename) {
		File f = new File(filename);
		if (f.exists())
			return ;
  	try (FileOutputStream fos = new FileOutputStream(filename)) {
   		fos.write(generateSalt());
		}
		catch (Exception e)
		{
			// add some error check here
		}
	}

	private byte[] readSaltFile(String filename) {
		byte[] ret;
		
		try 
		{
			ret= Files.readAllBytes(new File(filename).toPath());
		}
		catch (Exception e)
		{
			//add some error check here
			return new byte[0];
		}	
		return ret;
}


	private byte[] generateSalt() {
		SecureRandom r = new SecureRandom();
		byte[] salt = new byte[20];
		r.nextBytes(salt);

		return salt;
	}
	public boolean setKey(String key, String value) {

		try {
			byte[] salt = readSaltFile(filename+".salt");
			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBE");
			SecretKey generatedSecret = factory.generateSecret(new PBEKeySpec(value.toCharArray(), salt, 100, 512));

			KeyStore ks = KeyStore.getInstance("JCEKS");
			ks.load(null, password.toCharArray());
			KeyStore.PasswordProtection keyStorePP = new KeyStore.PasswordProtection(password.toCharArray());

			ks.setEntry(key, new KeyStore.SecretKeyEntry( generatedSecret), keyStorePP);

			FileOutputStream fos = new java.io.FileOutputStream(filename);
			ks.store(fos, password.toCharArray());
			return true;
		}
		catch (Exception e) {	
			System.err.println(e.getMessage());
			return false;
		}
	}

	public boolean hasKey(String key) {
		try 
		{
			getKey(key);
			return true;
		}
		catch (Exception e) {
			return false;	
		}


	}

	public String getKey(String key) {

		try 
		{

			KeyStore ks = KeyStore.getInstance("JCEKS");
			ks.load(null, password.toCharArray());
			KeyStore.PasswordProtection keyStorePP = new KeyStore.PasswordProtection(password.toCharArray());

			FileInputStream fIn = new FileInputStream(filename);
			ks.load(fIn, password.toCharArray());

			SecretKeyFactory factory = SecretKeyFactory.getInstance("PBE");

			KeyStore.SecretKeyEntry ske =
				(KeyStore.SecretKeyEntry)ks.getEntry(key, keyStorePP);

			PBEKeySpec keySpec = (PBEKeySpec)factory.getKeySpec(
					ske.getSecretKey(),
					PBEKeySpec.class);

			char[] password = keySpec.getPassword();

			return new String(password);
		}
		catch (Exception e) 
		{
			return null;
		}
	}

}
